__precompile__()

module PairSets

export PairSet, PairSetFinal
export init_eulerian
export compute, init_hdf5, save_pair_set
export save_results, load_results
export load_mean_profile
export load_dissipation, interpolate

import Base: +, eltype

using NadiaSpectral: DonneesNS
using NadiaSpectral.Cheb: ChebDiff
using ParticleIO
using LagrangianStats: PData

using HDF5
using JLD
using ProgressMeter

const WALL_UNITS = false
const FILE_STATS = "stats.h5"


"""
Physical dimensions of a quantity.
"""
struct PhysDims
    L :: Int  # length
    T :: Int  # time
    M :: Int  # mass
end

PhysDims(; L=0, T=0, M=0) = PhysDims(L, T, M)

"Example: `PhysDims(Dict('L' => 1, 'T' => -2))` for acceleration."
function PhysDims(D::Dict{Char,Int})
    L = haskey(D, 'L') ? D['L'] : 0
    T = haskey(D, 'T') ? D['T'] : 0
    M = haskey(D, 'M') ? D['M'] : 0
    PhysDims(L, T, M)
end


"Holds the profile of a given quantity along the channel width."
struct ProfileY{T}
    Nc    :: Int  # number of components
    Ny    :: Int  # number of wall distances
    y     :: Vector{T}  # [Ny]
    data  :: Matrix{T}  # [Ny, Nc]
    pdims :: PhysDims
    function ProfileY{T}(y, data::Matrix, pdims::PhysDims) where T
        Ny, Nc = size(data)
        @assert length(y) == Ny
        new(Nc, Ny, y, data, pdims)
    end
    ProfileY{T}(y, data::Matrix, ddims::Dict{Char,Int}) where T =
        ProfileY(y, data, PhysDims(ddims))
    ProfileY{T}(y, data::Vector, args...) where T =
        ProfileY(y, reshape(data, length(data), 1), args...)
end


ProfileY{T}(y::Vector{T}, args...) = ProfileY{T}(y, args...)

eltype{T}(::ProfileY{T}) = T

# Note on 2D tensors
# ==================
#
# Symmetric tensors of the form R_{ij} (with i, j ∈ {x, y, z}) are treated as 1D
# arrays with 6 components.
#
# The order of components is: {xx, yy, zz, xy, yz, zx}.

"""
Particle pair statistics at a given (initial) instant.
"""
struct InitialPairStats
    y0    :: Float64          # average wall-distance of first particle
    dUdy  :: Vector{Float64}  # mean velocity gradient dU_i/dy (average between
                              # the positions of the 2 particles)
    D     :: Vector{Float64}  # <  D_i  >  [3]
    dv    :: Vector{Float64}  # < δv_i  >  [3]
    da    :: Vector{Float64}  # < δa_i  >  [3]
    dU    :: Vector{Float64}  # < δU_i  >  [3]
    dA    :: Vector{Float64}  # < δA_i  >  [3]
    D_D   :: Vector{Float64}  # <  D_i *  D_j >  [6]
    D_dv  :: Matrix{Float64}  # <  D_i * δv_j >  [3, 3]
    D_da  :: Matrix{Float64}  # <  D_i * δa_j >  [3, 3]
    dv_dv   :: Vector{Float64}  # < δv_i  * δv_j  >  [6]
    dvp_dvp :: Vector{Float64}  # < δv_i' * δv_j' >  [6]
    dv_da   :: Matrix{Float64}  # < δv_i  * δa_j  >  [3, 3]
    dvp_dap :: Matrix{Float64}  # < δv_i' * δa_j' >  [3, 3]
end


abstract type AbstractPairSet end

"""
Set of particle pairs.

Data associated to a set of particle pairs initially located at a single
wall-normal location y_0 (actually, this corresponds to the location of the
first particle), and whose initial separation is determined by a single
separation vector D⃗0 relative to the first particle.

All quantities are expressed in simulation units.

Some definitions
----------------

    R_i  = D_i - D_{0i}

    R_i' = ∫ δv_i'(τ) dτ  (pair separation due to fluctuating velocity)

"""
struct PairSet{T<:AbstractFloat} <: AbstractPairSet
    Npairs :: Int           # number of particle pairs
    pairs :: Array{Int,2}   # particle ids [2, Npairs]
    # Wall-normal location index of each particle [Nparticles].
    # For each particle, associates its wall-normal position with an index of
    # `Umean.y`. Used to speed-up interpolations of the mean velocity field.
    # This vector is meant to be shared between multiple `PairSet` instances.
    # Both `yindex` and `Umean` can be initialised by calling `init_eulerian`.
    yindex :: Vector{Int}
    Umean :: ProfileY{Float64}    # mean velocity profile (3 components)
    Rmean_prev :: Matrix{Float64} # separation by mean velocity at previous timestep [3, Npairs]
    dU_prev :: Matrix{Float64}    # mean velocity difference δU at previous timestep [3, Npairs]
    # Pair separation at the previous iteration [Npairs].
    # Used to take into account domain periodicity.
    dx_prev :: Vector{T}    # [Npairs]
    dz_prev :: Vector{T}    # [Npairs]
    dv0 :: Matrix{Float64}  # initial relative velocity δv0 [3, Npairs]
    y0_nominal :: Float64          # nominal initial wall distance y_0
    D0_nominal :: Vector{Float64}  # nominal initial separation vector
                                   # ([1] if pairs are not oriented, else [3])
    D0_pairs :: Matrix{T}    # actual initial separation vector of each pair [3, Npairs]
    itrange :: StepRange{Int,Int}   # iteration range
    Nt :: Int                # number of time steps
    Dt :: Float64            # time between two time steps
    initial_stats :: InitialPairStats
    D2 :: Array{Float64,2}      # < D_i * D_j > [6, Nt]
    R2 :: Array{Float64,2}      # < R_i * R_j > [6, Nt]
    R2p :: Array{Float64,2}     # < R_i' * R_j' > [6, Nt]
    dv0_dv :: Array{Float64,3}  # < δv0_i ⋅ δv_j(t) >  [3, 3, Nt]
end


"""
Initialises the Eulerian mean velocity profile and computes the y-index of each
particle in the Eulerian grid.

The returned values correspond to the fields `yindex` and `Umean` of `PairSet`.
"""
function init_eulerian(pdata::PData)
    Umean = load_mean_profile("velocity")
    yindex = Vector{Int}(pdata.Npart)
    @assert issorted(Umean.y)
    for n = 1:pdata.Npart
        yindex[n] = searchsortedlast(Umean.y, pdata.pos[2, n])
    end
    yindex, Umean
end


"""
Contains a subset of `PairSet` fields.

Similar to `PairSet`, but excludes arrays that are only needed during
computation of pair statistics and that can be discarded later.

This can be useful for saving results to files.
"""
struct PairSetFinal <: AbstractPairSet
    Npairs :: Int  # number of particle pairs (i.e. number of samples)
    y0_nominal :: Float64          # nominal initial wall distance y_0
    D0_nominal :: Vector{Float64}  # nominal initial separation vector
                                   # ([1] if pairs are not oriented, else [3])
    Nt :: Int                # number of time steps
    Dt :: Float64            # time between two time steps
    initial_stats :: InitialPairStats
    D2 :: Array{Float64,2}      # < D_i * D_j > [6, Nt]
    R2 :: Array{Float64,2}      # < R_i * R_j > [6, Nt]
    R2p :: Array{Float64,2}     # < R_i' * R_j' > [6, Nt]
    dv0_dv :: Array{Float64,3}  # < δv0_i ⋅ δv_j(t) >  [3, 3, Nt]
end

PairSetFinal(P::PairSet) =
PairSetFinal(P.Npairs, P.y0_nominal, P.D0_nominal, P.Nt, P.Dt, P.initial_stats,
             P.D2, P.R2, P.R2p, P.dv0_dv)


"""
Create PairSet from another PairSet object.

The number of time iterations contained in `P` must be inferior or equal to the
length of `itrange`.

This can be used to resume the computation of dispersion stats from previous
results obtained from a smaller iteration range.
"""
function PairSet(P::PairSet, itrange::StepRange)
    Nt_in = length(P.itrange)
    Nt = length(itrange)
    @assert Nt >= Nt_in
    D2 = zeros(6, Nt)
    R2 = copy(D2)
    R2p = copy(D2)
    dv0_dv = zeros(3, 3, Nt)
    Pnew = PairSet(P.Npairs, P.pairs, P.yindex, P.Umean, P.Rmean_prev,
                   P.dU_prev, P.dx_prev, P.dz_prev, P.dv0, P.y0_nominal,
                   P.D0_nominal, P.D0_pairs, itrange, Nt, P.Dt, P.initial_stats,
                   D2, R2, R2p, dv0_dv)
    # Copy all available temporal data to P (i.e. up to iteration Nt_in).
    for s in (:D2, :R2, :R2p, :dv0_dv)
        A = getfield(P, s)
        Anew = getfield(Pnew, s)
        for i = 1:length(A)
            Anew[i] = A[i]
        end
    end
    Pnew
end


"""
Construct and initialise PairSet object.

Some parameters:

- `Umean` is a mean velocity profile along the channel width.

- `yindex` represents the instantaneous wall-normal location of each particle in
the `Umean.y` vector, such that the n-th particle is located in `Umean.y[j] ≤ y
< Umean.y[j+1]`, where `j = yindex[n]` and `y = pdata.pos[2, n]` is the
wall-normal particle position.

"""
function PairSet(pdata::PData, pairs_in::Array{Int,2}, Lxyz_in::Vector{Float64},
                 y0_nominal::Real, D0_nominal_in::Vector, itrange, Dt,
                 Umean::ProfileY, yindex::Vector{Int})
    pairs = copy(pairs_in)
    D0_nominal = copy(D0_nominal_in)

    # Note: D0_nominal can be a one-component "vector" when particle pairs are
    # not oriented in a predefined way.
    @assert length(D0_nominal) in (1, 3)
    const oriented = length(D0_nominal) == 3

    @assert size(pairs, 1) == 2
    Npairs = size(pairs, 2)
    Nt = length(itrange)

    D2 = zeros(6, Nt)
    R2 = zeros(6, Nt)
    R2p = zeros(6, Nt)
    dv0_dv = zeros(3, 3, Nt)

    # Some verifications...
    @assert length(yindex) == pdata.Npart
    for n = 1:pdata.Npart
        j = yindex[n]
        @assert Umean.y[j] ≤ pdata.pos[2, n] < Umean.y[j + 1]
    end

    Rmean_prev = zeros(3, Npairs)
    dU_prev = zeros(3, Npairs)

    T = eltype(PData)
    dx_prev = Vector{T}(Npairs)
    dz_prev = similar(dx_prev)
    D0_pairs = Matrix{T}(3, Npairs)
    dv0 = Matrix{Float64}(3, Npairs)

    Lxyz = T.(Lxyz_in)
    Lh = Lxyz./2

    for n = 1:Npairs
        p = pairs[1, n]
        q = pairs[2, n]
        @assert !oriented || pdata.pos[2, p] ≈ y0_nominal

        for i = 1:3
            dr = pdata.pos[i, q] - pdata.pos[i, p]
            dv = pdata.vel[i, q] - pdata.vel[i, p]
            da = pdata.acc[i, q] - pdata.acc[i, p]

            if i != 2
                # Correct for periodicity.
                dr = deperiodise_initial(dr, Lxyz[i], Lh[i])
            end
            @assert !oriented || isapprox(dr, D0_nominal[i], rtol=1e-3)

            dv0[i, n] = dv
            D0_pairs[i, n] = dr

            if i == 1
                dx_prev[n] = dr
            elseif i == 3
                dz_prev[n] = dr
            end
        end
    end

    initial_stats = InitialPairStats(pdata, pairs, Lxyz_in, yindex)
    PairSet(Npairs, pairs, yindex, Umean, Rmean_prev, dU_prev, dx_prev, dz_prev,
            dv0, y0_nominal, D0_nominal, D0_pairs, itrange, Nt, Dt,
            initial_stats, D2, R2, R2p, dv0_dv)
end


"""
Revert periodic boundary conditions on the initial separation of a particle
pair.

`dx` is the distance between the two particles in one of the periodic
dimensions, according to the input particle data. If the two particles are
located very near a periodic boundary, at opposing sides (e.g. `x1 = ε` and `x2
= Lx - ε`, where `Lx` is the domain length in that direction and `ε << Lx`),
then `|dx|` will be very large (close to `Lx`), while the actual physical
distance will be much smaller (equal to `Lx - |dx|`). This function returns the
particle separation corrected by the effect of periodic boundaries.

`dx_max` is a positive threshold such that the input distance is considered as
"too large", and thus needs to be corrected. A reasonable value is `dx_max =
Lx/2`.

"""
@inline function deperiodise_initial{T}(dx::T, Lx::T, dx_max::T)
    @assert -Lx < dx < Lx
    if dx > dx_max
        dx -= Lx
    elseif dx < -dx_max
        dx += Lx
    end
    @assert abs(dx) < dx_max
    dx
end


"""
Derivative of a profile (using Chebyshev decomposition).
"""
function derive(u::ProfileY)
    dudy = copy(u.data)
    for c = 1:u.Nc
        dudy[:, c] .= ChebDiff(u.data[:, c], u.y)
    end
    D = u.pdims
    pdims = PhysDims(L=D.L - 1, T=D.T, M=D.M)
    ProfileY(u.y, dudy, pdims)
end


"""
Construct InitialPairStats object from particle pair data.
"""
function InitialPairStats(pdata::PData, pairs::Matrix{Int},
                          Lxyz_in::Vector{Float64}, yindex::Vector{Int})
    # Initialise fields.
    y0 = 0.0
    dUdy = zeros(3)  # dUx/dy, dUy/dy, dUz/dz
    D = zeros(3)
    dv, da = zeros(3), zeros(3)
    dU, dA = zeros(3), zeros(3)
    D_D = zeros(6)
    D_dv, D_da = zeros(3,3), zeros(3,3)
    dv_dv, dvp_dvp = zeros(6), zeros(6)
    dv_da, dvp_dap = zeros(3,3), zeros(3,3)

    T = eltype(PData)
    Dr = Vector{T}(3)
    Dv = Vector{T}(3)
    Da = Vector{T}(3)
    DU = Vector{T}(3)
    DA = Vector{T}(3)  # A[i] = v_y * dU[i]/dy
    Lxyz = T.(Lxyz_in)
    Lh = Lxyz./2

    # Indices for symmetric tensors written as 1D arrays.
    ij_indices = [(1, 1), (2, 2), (3, 3),  # xx, yy, zz, xy, yz, zx
                  (1, 2), (2, 3), (3, 1)]

    # Mean velocity and mean shear profiles.
    U_prof = load_mean_profile("velocity")
    dUdy_prof = derive(U_prof)

    # Arrays that hold interpolated U and dU/dy vectors at each particle
    # position.
    U_p = similar(U_prof.data, 3)  # mean velocity at position of particle `p`
    U_q = copy(U_p)
    dUdy_p = similar(dUdy_prof.data, 3)  # dU[i]/dy at position of particle `p`
    dUdy_q = copy(dUdy_p)

    @assert size(pairs, 1) == 2
    Npairs = size(pairs, 2)
    for n = 1:Npairs
        p = pairs[1, n]
        q = pairs[2, n]

        y_p = pdata.pos[2, p]
        y_q = pdata.pos[2, q]
        vy_p = pdata.vel[2, p]
        vy_q = pdata.vel[2, q]

        y0 += y_p

        interpolate!(U_p, U_prof, y_p, yindex[p])
        interpolate!(U_q, U_prof, y_q, yindex[q])
        interpolate!(dUdy_p, dUdy_prof, y_p, yindex[p])
        interpolate!(dUdy_q, dUdy_prof, y_q, yindex[q])

        for i = 1:3
            Dr[i] = pdata.pos[i, q] - pdata.pos[i, p]
            if i != 2
                # Correct for periodicity.
                Dr[i] = deperiodise_initial(Dr[i], Lxyz[i], Lh[i])
            end

            Dv[i] = pdata.vel[i, q] - pdata.vel[i, p]
            Da[i] = pdata.acc[i, q] - pdata.acc[i, p]
            DU[i] = U_q[i] - U_p[i]
            DA[i] = vy_q * dUdy_q[i] - vy_p * dUdy_p[i]

            dUdy[i] += 0.5 * (dUdy_p[i] + dUdy_q[i])
            D[i] += Dr[i]
            dv[i] += Dv[i]
            da[i] += Da[i]
            dU[i] += DU[i]
            dA[i] += DA[i]
        end

        # Symmetric tensors.
        for k = 1:6
            i, j = ij_indices[k]
            D_D[k] += Dr[i] * Dr[j]
            dv_dv[k] += Dv[i] * Dv[j]
            dvp_dvp[k] += (Dv[i] - DU[i]) * (Dv[j] - DU[j])
        end

        # Non-symmetric tensors.
        for j = 1:3, i = 1:3
            D_dv[i, j] += Dr[i] * Dv[j]
            D_da[i, j] += Dr[i] * Da[j]
            dv_da[i, j] += Dv[i] * Da[j]
            dvp_dap[i, j] += (Dv[i] - DU[i]) * (Da[j] - DA[j])
        end
    end

    # Divide by total number of samples.
    Ninv = 1.0 / Npairs
    y0 *= Ninv
    X = InitialPairStats(y0, dUdy, D, dv, da, dU, dA, D_D, D_dv, D_da, dv_dv,
                         dvp_dvp, dv_da, dvp_dap)
    for f in fieldnames(InitialPairStats)
        f === :y0 && continue
        @assert fieldtype(InitialPairStats, f) <: Array
        v = getfield(X, f)
        v .*= Ninv
    end

    X :: InitialPairStats
end


"""Sum statistical data from PairSetFinal objects."""
function +(P::PairSetFinal, Q::PairSetFinal)
    @assert P.y0_nominal == Q.y0_nominal
    @assert P.D0_nominal ≈ Q.D0_nominal
    @assert P.Nt == Q.Nt
    @assert P.Dt == Q.Dt
    const Np = P.Npairs
    const Nq = Q.Npairs
    add(p, q) = (Np .* p .+ Nq .* q) ./ (Np + Nq)
    istats = deepcopy(P.initial_stats)
    for s in fieldnames(istats)
        s === :y0 && continue
        x = getfield(istats, s)
        @assert x isa Array
        xnew = add(getfield(P.initial_stats, s), getfield(Q.initial_stats, s))
        copy!(x, xnew)
    end
    PairSetFinal(
        P.Npairs + Q.Npairs, P.y0_nominal, P.D0_nominal, P.Nt, P.Dt, istats,
        add(P.D2, Q.D2),
        add(P.R2, Q.R2),
        add(P.R2p, Q.R2p),
        add(P.dv0_dv, Q.dv0_dv)
       )
end


"""Properties of a dataset.

Used for saving datasets to HDF5 file.
"""
struct DatasetProps
    name :: String  # HDF5 dataset name
    description :: String
    pdims :: PhysDims
end


DatasetProps(name, description; kwargs...) =
    DatasetProps(name, description, PhysDims(; kwargs...))


function to_wall_units!(P::ProfileY, utau, nu)
    @assert P.y[1] ≈ -1.0 && P.y[end] ≈ 1.0
    D = P.pdims
    @assert D.M == 0
    L = nu / utau
    T = nu / (utau * utau)
    P.y .+= 1.0  # ∈ [0, 2]
    scale!(P.y, 1.0 / L)
    scale!(P.data, 1.0 / (L^D.L * T^D.T))
    nothing
end

"""
Value of a ProfileY averaged between two wall-normal locations y1, y2.
"""
mid_value(U::ProfileY, y1::Real, y2::Real) =
    0.5 * (interpolate(U, y1) .+ interpolate(U, y2)) :: Vector

"""
Load mean turbulent dissipation tensor profile ɛ_ij(y) from Eulerian statistics.
"""
function load_dissipation(; tensor=false, total=false)
    # Stats are expected to be in simulation units and cover the whole channel.
    gpath::String = "/Dissipation" * (total ? "_total" : "")
    local y::Vector{Float64}, ε::Matrix{Float64}
    h5open(FILE_STATS, "r") do ff
        g = ff[gpath]
        y = read(g["y"])
        Ny = length(y)
        @assert y[1] ≈ -1.0 && y[end] ≈ 1.0
        if tensor
            ε = read(g["eps_ij"])
            @assert size(ε) == (Ny, 6)
        else
            ε = reshape(read(g["eps"]), Ny, 1)
        end
    end
    # writedlm("test_eps.dat", [y ɛ])
    ProfileY(y, ɛ, Dict('L' => 2, 'T' => -3))
end


"Load velocity or acceleration mean profile."
function load_mean_profile(varname)
    @assert varname ∈ ("velocity", "acceleration")
    gpath = "/Moments/$varname"
    local y::Vector{Float64}, avg::Matrix{Float64}
    h5open(FILE_STATS, "r") do ff
        g = ff[gpath]
        y = read(g["y"])
        avg = g["mean"][:, 1:3]
        Nyh = div(length(y)+1, 2)
        @assert y[1] ≈ -1.0 && y[end] ≈ 1.0 && abs(y[Nyh]) < 1e-15
        @assert (varname != "velocity") || (0.9 < avg[Nyh, 1] < 1.1)
    end
    T = Dict("velocity" => -1, "acceleration" => -2)[varname]
    ProfileY(y, avg, Dict('L' => 1, 'T' => T))
end


const _datasets_InitialPairStats =
    Dict(
         # :y0 =>
         # DatasetProps("<y1>", "< y_1 >", L=1),
         :dUdy =>
         DatasetProps("dU_dy", "< dU_i/dy >", T=-1),
         :D =>
         DatasetProps("<D0>", "< D0 >", L=1),
         :dv =>
         DatasetProps("<dv0>", "< δv0 >", L=1, T=-1),
         :da =>
         DatasetProps("<da0>", "< δa0 >", L=1, T=-2),
         :dU =>
         DatasetProps("<dU0>", "< δU0 >", L=1, T=-1),
         :dA =>
         DatasetProps("<dA0>", "< δA0 >", L=1, T=-2),
         :D_D =>
         DatasetProps("<D0 * D0>", "< D0 ⋅ D0 >", L=2),
         :D_dv =>
         DatasetProps("<D0 * dv0>", "< D0 ⋅ δv0 >", L=2, T=-1),
         :D_da =>
         DatasetProps("<D0 * da0>", "< D0 ⋅ δa0 >", L=2, T=-2),
         :dv_dv =>
         DatasetProps("<dv0 * dv0>", "< δv0 ⋅ δv0 >", L=2, T=-2),
         :dvp_dvp =>
         DatasetProps("<dv0' * dv0'>", "< δv0' ⋅ δv0' >", L=2, T=-2),
         :dv_da =>
         DatasetProps("<dv0 * da0>", "< δv0 ⋅ δa0 >", L=2, T=-3),
         :dvp_dap =>
         DatasetProps("<dv0' * da0'>", "< δv0' ⋅ δa0' >", L=2, T=-3),
        )

const _datasets_PairSet =
    Dict(:R2 =>
         DatasetProps("<R^2>", "< R(t)^2 >", L=2),
         :R2p =>
         DatasetProps("<R'^2>", "< R'(t)^2 >", L=2),
         # :dv0_dv =>
         # DatasetProps("<dv0 * dv>", "< δv0 ⋅ δv(t) >", L=2, T=-2),
         :D2 =>
         DatasetProps("<D^2>", "< D(t)^2 >", L=2),
        )

datasets(::InitialPairStats) = _datasets_InitialPairStats
datasets(::AbstractPairSet) = _datasets_PairSet

@inline function get_new_wall_index(j::Int, ygrid::Vector, y::Real)
    while ygrid[j] > y
        j -= 1
    end
    while ygrid[j+1] <= y
        j += 1
    end
    j
end

function update_wall_index!(yindex::Vector{Int}, ygrid::Vector,
                            ypart::AbstractVector)
    # @assert length(yindex) == length(ypart)
    for n = 1:length(ypart)
        yindex[n] = get_new_wall_index(yindex[n], ygrid, ypart[n])
    end
    nothing
end

@inline function interpolate!(out::Array, U::ProfileY, y::Real, j::Int)
    @assert U.y[j] ≤ y < U.y[j+1]
    # Linear interpolation.
    α = (U.y[j+1] - y) / (U.y[j+1] - U.y[j])
    β = 1 - α
    for c = 1:U.Nc
        out[c] = α * U.data[j, c] + β * U.data[j+1, c]
    end
    nothing
end

# Slower variant of `interpolate!`.
function interpolate(U::ProfileY, y::Real)
    out = similar(U.data, U.Nc)
    j = searchsortedlast(U.y, y)
    interpolate!(out, U, y, j)
    out
end

function update!{T}(pair_sets::Array{PairSet{T}}, pdata::PData, Lxyz::Vector,
                    timestep_index::Integer)
    # Update wall-normal position index for all particles.
    # Assumes that all PairSets share the same yindex instance.
    P = pair_sets[1]
    update_wall_index!(P.yindex, P.Umean.y, @view pdata.pos[2, :])

    # @code_warntype update!(pair_sets[1], pdata, Lxyz, timestep_index)
    Threads.@threads for P in pair_sets
        update!(P, pdata, Lxyz, timestep_index)
    end
    nothing
end


function update!(P::PairSet, pdata::PData, Lxyz::Vector,
                 timestep_index::Integer)
    const pos = pdata.pos
    const vel = pdata.vel
    const m = timestep_index

    const PFloat = eltype(PData)
    Lx::PFloat = Lxyz[1]
    Lz::PFloat = Lxyz[3]
    Lxh = 0.5Lx
    Lzh = 0.5Lz

    # Set sums to zero.
    for k = 1:6
        P.D2[k, m] = 0.0
        P.R2[k, m] = 0.0
        P.R2p[k, m] = 0.0
    end
    for j = 1:3, i = 1:3
        P.dv0_dv[i, j, m] = 0.0
    end

    const dt_half = 0.5 * P.Dt

    Dr = Vector{Float64}(3)
    Dv = Vector{Float64}(3)

    R = Vector{Float64}(3)   # separation by total relative velocity
    Rp = Vector{Float64}(3)  # separation by fluctuating relative velocity

    Ua = Vector{Float64}(3)  # mean velocity field at position of particle `a`
    Ub = Vector{Float64}(3)

    for n = 1:P.Npairs
        a = P.pairs[1, n]
        b = P.pairs[2, n]

        # Compute particle separation in the 3 directions, taking into account
        # periodicity in x and z.
        # Periodicity may cause problems at the moment when one of the two
        # particles wraps around the domain. In that case, the computed distance
        # δx between the two particles increases significantly compared to the
        # previous timestep. Thus, this problem can be detected and corrected by
        # comparing the current computed distance, δx(t), to the previous one,
        # δx(t-Δt). If their difference is too large, it means that one of the
        # particles wrapped around the domain, and the value of δx(t) is
        # corrected accordingly.

        # 1. Separation in x.
        dx = pos[1, b] - pos[1, a]
        while dx - P.dx_prev[n] > Lxh
            dx -= Lx
        end
        while P.dx_prev[n] - dx > Lxh
            dx += Lx
        end
        # @assert abs(dx - P.dx_prev[n]) < Lxh
        P.dx_prev[n] = dx
        Dr[1] = dx

        # 2. Separation in y.
        dy = pos[2, b] - pos[2, a]
        Dr[2] = dy

        # 3. Separation in z.
        dz = pos[3, b] - pos[3, a]
        while dz - P.dz_prev[n] > Lzh
            dz -= Lz
        end
        while P.dz_prev[n] - dz > Lzh
            dz += Lz
        end
        # @assert abs(dz - P.dz_prev[n]) < Lzh
        P.dz_prev[n] = dz
        Dr[3] = dz

        y_a = pos[2, a]  # wall-normal position of particle `a`
        y_b = pos[2, b]
        interpolate!(Ua, P.Umean, y_a, P.yindex[a])
        interpolate!(Ub, P.Umean, y_b, P.yindex[b])

        for i = 1:3
            Dv[i] = vel[i, b] - vel[i, a]
            # Update separation by mean velocity field using explicit centred
            # scheme: R[m] = R[m-1] + (δU[m-1] + δU[m]) Δt / 2
            δU = Ub[i] - Ua[i]
            P.Rmean_prev[i, n] += dt_half * (P.dU_prev[i, n] + δU)
            P.dU_prev[i, n] = δU
            R[i] = Dr[i] - P.D0_pairs[i, n]    # R(t) = D(t) - D(0)
            Rp[i] = R[i] - P.Rmean_prev[i, n]  # R'(t) = R(t) - Rmean(t)
        end

        # Initial condition: R(0) = R'(0) = Rmean(0) = 0.
        # In the case of R(0), this is already enforced by the value of
        # P.D0_pairs[:, n] which is supposed to be equal to Dr[:].
        if m == 1
            for i = 1:3
                @assert R[i] ≈ 0.0
                Rp[i] = 0.0
                P.Rmean_prev[i, n] = 0.0
            end
        end

        # Update sums.
        for k = 1:6
            i = TENSOR_INDEX_I[k]
            j = TENSOR_INDEX_J[k]
            P.D2[k, m] += Dr[i] * Dr[j]
            P.R2[k, m] += R[i] * R[j]
            P.R2p[k, m] += Rp[i] * Rp[j]
        end
        for j = 1:3, i = 1:3
            P.dv0_dv[i, j, m] += P.dv0[i, n] * Dv[j]
        end
    end

    # Average sums.
    Ninv::Float64 = 1.0 / P.Npairs
    for k = 1:6
        P.D2[k, m] *= Ninv
        P.R2[k, m] *= Ninv
        P.R2p[k, m] *= Ninv
    end
    for j = 1:3, i = 1:3
        P.dv0_dv[i, j, m] *= Ninv
    end

    nothing
end


"""
Compute pair dispersion statistics.

Statistics are computed in the iteration range `itrange`.

`pair_sets_in` is a PairSet array. If the number of iterations in the PairSets
is inferior to that of the iteration range, it is assumed that the input
PairSets contains (incomplete) results from a previous computation. Otherwise,
the PairSet array must contain initialised but not yet computed objects, and it
is overwritten by this function.

"""
function compute{T}(D::Donnees, particle_dirs, itrange::StepRange,
                    pair_sets_in::Array{PairSet{T}})
    # Read data at initial iteration.
    pfile = particle_file(particle_dirs, itrange.start)
    const pdata = PData(pfile)
    const Nt = length(itrange)

    if Nt > pair_sets_in[1].Nt
        # Input contains incomplete results from previous computation.
        # Create larger PairSet array and copy available data.
        pair_sets = similar(pair_sets_in)
        @time for i in eachindex(pair_sets)
            pair_sets[i] = PairSet(pair_sets_in[i], itrange)
        end
        iters_in = pair_sets_in[1].itrange
        Nt_in = length(iters_in)
        @assert all(P.itrange == iters_in for P in pair_sets_in)
        @assert Nt_in < Nt
        @assert itrange[1:Nt_in] == iters_in
        iter_start = Nt_in + 1
    else
        # Input contains non-computed data (i.e. almost everything is zero).
        # Input is overwritten with results.
        pair_sets = pair_sets_in
        iter_start = 1
    end

    # For deperiodisation of trajectories in x and z.
    Lxyz = get_domain_dimensions(D)

    # @code_warntype update!(pair_sets, pdata, Lxyz, Nt)

    @showprogress "Range $(itrange)..." for m = iter_start:Nt
        pfile = particle_file(particle_dirs, itrange[m])
        h5open(pfile, "r") do io
            read_field!(pdata.pos, io, "position")
            read_field!(pdata.vel, io, "velocity")
        end
        update!(pair_sets, pdata, Lxyz, m)
    end

    pair_sets
end

function save_results(pairs::Array{T} where T<:AbstractPairSet, filename)
    jldopen(filename, "w", compress=true) do io
        write(io, "pair_dispersion", pairs)
    end
    info("Saved $filename")
    nothing
end

load_results(filename) = load(filename, "pair_dispersion")

"Return trace of 2D symmetric tensor under its 1D representation."
function sym_tensor_trace{T}(v::Array{T,1}) :: T
    @assert length(v) == 6
    v[1] + v[2] + v[3]
end

function sym_tensor_trace{T}(v::Array{T,2}) :: Array{T,1}
    @assert size(v, 1) == 6
    tr = zeros(T, size(v, 2))
    for n in eachindex(tr), i = 1:3
        tr[n] += v[i, n]
    end
    tr
end

"Return trace of 2D general (non-symmetric) tensor."
function nonsym_tensor_trace{T}(v::Array{T,2}) :: T
    @assert size(v) == (3, 3)
    trace(v)
end

function nonsym_tensor_trace{T}(v::Array{T,3}) :: Array{T,1}
    @assert size(v, 1) == size(v, 2) == 3
    tr = zeros(T, size(v, 3))
    for n in eachindex(tr), i = 1:3
        tr[n] += v[i, i, n]
    end
    tr
end

h5write_dataset(g::HDF5Group, name::String, v::Number) =
    g[name] = v

"Write compressed dataset to HDF5 file."
h5write_dataset(g::HDF5Group, name::String, v::Vector) =
    g[name, "chunk", size(v), "shuffle", (), "deflate", 6] = v

function h5write_dataset(g::HDF5Group, name::String, v::Array)
    chunks = collect(size(v))
    chunks[1] = 1
    g[name, "chunk", chunks, "shuffle", (), "deflate", 6] = v
end

# Switch from 1D (k) to 2D symmetric tensor indices (i, j).
const TENSOR_INDEX_I = (1, 2, 3, 1, 2, 3)
const TENSOR_INDEX_J = (1, 2, 3, 2, 3, 1)

"""
Initialises output HDF5 file with general parameters and data.

Actual results must be written to separate HDF5 groups with save_pair_set().
"""
function init_hdf5(io::HDF5File, P::AbstractPairSet, utau, nu)
    attrs(io)["README.tensors"] =
    """
    Some of the datasets included describe 3x3 symmetric tensors of the form
    R_{ij} (with i, j ∈ {x, y, z}).

    These symmetric tensors are stored as 1D arrays with 6 components. The
    order of components is {xx, yy, zz, xy, yz, zx}.
    """
    attrs(io)["wall_units"] = Int(WALL_UNITS)  # 0 or 1
    attrs(io)["README.dimensions"] =
        ifelse(WALL_UNITS,
               "All dimensional quantities are in wall units (normalised by u_τ and ν).",
               "All dimensional quantities are in simulation units (normalised by Usim and h).")

    # Write u_τ and ν (these are = 1 in wall units).
    io["u_tau"] = ifelse(WALL_UNITS, 1.0, utau) :: Float64
    io["nu"] = ifelse(WALL_UNITS, 1.0, nu) :: Float64

    # Write time.
    Dt = P.Dt * (WALL_UNITS ? utau*utau/nu : 1.0)
    io["tau"] = collect(Dt * (0:P.Nt-1))
    attrs(io["tau"])["Description"] = "Computed time lags τ"

    nothing
end

"""
Save results associated to a given pair to a HDF5 group.
"""
function save_pair_set(g::HDF5Group, P::AbstractPairSet, utau, nu)
    groupname = splitdir(name(g))[2]

    attrs(g)["num_samples"] = P.Npairs

    # Create soft link to /tau
    HDF5.h5l_create_soft("/tau", g, "tau", HDF5.H5P_DEFAULT, HDF5.H5P_DEFAULT)

    # Create soft link "<(D - D0)>^2" → "<R^2>" (for backwards compatibility).
    dname = datasets(P)[:R2].name
    @assert dname == "<R^2>"
    HDF5.h5l_create_soft(dname, g, "<(D - D0)^2>", HDF5.H5P_DEFAULT,
                         HDF5.H5P_DEFAULT)

    # These are all in simulation units:
    # TODO load profiles only once
    eps = load_dissipation(total=false)
    epsT = load_dissipation(total=true)
    eps_ij = load_dissipation(total=false, tensor=true)
    epsT_ij = load_dissipation(total=true, tensor=true)

    @assert 2 .* eps.data ≈ sum(view(eps_ij.data, :, 1:3), 2)
    @assert 2 .* epsT.data ≈ sum(view(epsT_ij.data, :, 1:3), 2)

    if WALL_UNITS
        # Convert profiles to wall units
        foreach(x -> to_wall_units!(x, utau, nu),
                (eps, epsT, eps_ij, epsT_ij, umean, amean))
    end

    # Save datasets
    for obj in (P, P.initial_stats)
        for (var::Symbol, props::DatasetProps) in datasets(obj)
            v = copy(getfield(obj, var)) :: Array
            if WALL_UNITS
                D = props.pdims
                L = nu / utau
                T = nu / (utau * utau)
                scale!(v, 1.0 / (L^D.L * T^D.T))
            end
            @assert P.Nt ∉ (3, 6)  # to avoid potential issues...
            if size(v, 1) == 6  # symmetric tensor
                # Save trace of `v` as `props.name`, and the whole `v` tensor as
                # "$(props_name)_ij".
                tr = sym_tensor_trace(v)
                h5write_dataset(g, props.name, tr)
                h5write_dataset(g, props.name * "_ij", v)
            elseif ndims(v) >= 2 && size(v, 1) == size(v, 2) == 3  # non-symmetric tensor
                tr = nonsym_tensor_trace(v)
                h5write_dataset(g, props.name, tr)
                h5write_dataset(g, props.name * "_ij", v)  # FIXME permute i-j components
            else
                h5write_dataset(g, props.name, v)
            end
            attrs(g[props.name])["Description"] = props.description
        end
    end

    # Average dissipation among the positions of both particles
    # in a pair.
    y0 = P.initial_stats.y0
    y1 = y0
    y2 = y0 + P.initial_stats.D[2]
    if WALL_UNITS
        y1 = (1.0 + y1) * utau / nu
        y2 = (1.0 + y2) * utau / nu
    end

    ɛ = mid_value(eps, y1, y2)[1]
    ɛ_T = mid_value(epsT, y1, y2)[1]
    ɛ_ij = mid_value(eps_ij, y1, y2) :: Vector
    ɛ_T_ij = mid_value(eps_ij, y1, y2) :: Vector

    g["epsilon"] = ɛ
    g["epsilon_T"] = ɛ_T
    g["epsilon_ij"] = ɛ_ij
    g["epsilon_T_ij"] = ɛ_T_ij

    nothing
end

end
