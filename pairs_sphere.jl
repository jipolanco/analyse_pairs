#!/usr/bin/env julia
#
# Forwards and backwards dispersion statistics of particle pairs randomly
# initialised. Pairs are identified over spheres (or spherical shells) centred at
# the position of one particle.

push!(LOAD_PATH, ".")
using NadiaSpectral: DonneesNS
using ParticleIO
using PairSets
using CellLists
import LagrangianStats: get_subranges
using HDF5

include("constants.jl")
const PARTICLE_DIRS = [joinpath(d, "particles") for d in DATA_DIRS]

info("Running on $(Threads.nthreads()) threads.")

const OUTPUT_BASE = "pairs_sphere"
const DIR_PARTIAL = joinpath(OUTDIR, "partial")
mkpath(DIR_PARTIAL)

results_file(itrange::StepRange) =
    joinpath(DIR_PARTIAL, "$(OUTPUT_BASE)_$(itrange).jld")

# Maximum initial separation (Kolmogorov units).
const D0_MAX_ETA = 16.

# Initial wall distance of particle pairs.
if TESTING
    const Y0_PLUS = [20., 60., 200.]
else
    const Y0_PLUS = [20., 60., 200., 600., 1000.]
end
const DY_ETA = 4.0

# Subdivide iteration ranges into temporal windows.
# Note: Δt⁺ ≈ 1/30 ≈ 0.0333 for the Re_tau = 1440 simulations.
if TESTING
    const IT_WINDOW = 2000
    const IT_START_DELTA = 5000
    const IT_STEP = 100
else
    const IT_WINDOW = 160000  # number of iterations in a window
    const IT_START_DELTA = 40000
    const IT_STEP = 10
end


# TODO use this:
"""Spatial configuration of spheres/spherical shells for particle pairs."""
struct SphereInitial
    D0min :: Float64  # inner radius of spherical shell (for now it's zero)
    D0max :: Float64  # outer radius of spherical shell
    y0    :: Float64  # initial wall distance of sphere centre: y0 ± dy
    dy    :: Float64
end

"Identify particle pairs from positions at a given iteration."
function initialise(D::Donnees, iters_fw::StepRange, iters_bw::StepRange)
    @assert iters_fw.start == iters_bw.start
    @assert length(iters_fw) == length(iters_bw)
    @assert iters_fw.step > 0 && iters_fw.step == -iters_bw.step

    it_zero = iters_fw.start
    pfile = particle_file(PARTICLE_DIRS, it_zero)
    pdata = PData(pfile)
    read_particle_file!(pdata, pfile)
    Lxyz = get_domain_dimensions(D)
    utau, nu = wall_parameters(D)

    Ny = length(Y0_PLUS)
    const T = eltype(PData)
    pairs_fw = Vector{PairSet{T}}(Ny)
    pairs_bw = Vector{PairSet{T}}(Ny)
    eps = load_dissipation(tensor=false, total=false)

    Dt_sim = get_deltat(D)
    Dt_fw = Dt_sim * iters_fw.step
    Dt_bw = Dt_sim * iters_bw.step
    @assert Dt_bw < 0

    yindex_fw, Umean = init_eulerian(pdata)
    yindex_bw = copy(yindex_fw)

    info("Identifying pairs at iteration $it_zero")
    @time for (j, y0_plus) in enumerate(Y0_PLUS)
        y0 = y0_plus * nu / utau - 1.0
        ε = interpolate(eps, y0)[1]
        η = (nu^3/ε)^0.25
        D0max = D0_MAX_ETA * η
        dy = DY_ETA * η
        pair_ids = identify_pairs(Lxyz, pdata.pos, D0max, y0=y0, dy=dy)
        D0_nominal = [D0max]
        pairs_fw[j] = PairSet(pdata, pair_ids, Lxyz, y0, D0_nominal, iters_fw,
                              Dt_fw, Umean, yindex_fw)
        pairs_bw[j] = PairSet(pdata, pair_ids, Lxyz, y0, D0_nominal, iters_bw,
                              Dt_bw, Umean, yindex_bw)
    end

    pairs_fw, pairs_bw
end

function compute_and_save{T}(D::Donnees, pairs_in::Array{PairSet{T}},
                             itrange::StepRange) :: Array{PairSetFinal}
    pairs = compute(D, PARTICLE_DIRS, itrange, pairs_in)
    pairs_final = [PairSetFinal(P) for P in pairs]
    save_results(pairs_final, results_file(itrange))
    pairs_final
end

function load_or_compute(D::Donnees, range_fw::StepRange,
                         range_bw::StepRange) :: NTuple{2,Array{PairSetFinal}}
    file_fw = results_file(range_fw)
    file_bw = results_file(range_bw)
    if !TESTING && isfile(file_fw) && isfile(file_bw)
        info("Loading JLD files for iteration $(range_fw.start)")
        pairs_fw = load_results(file_fw) :: Array{PairSetFinal}
        pairs_bw = load_results(file_bw) :: Array{PairSetFinal}
        return pairs_fw, pairs_bw
    end

    Pfw, Pbw = initialise(D, range_fw, range_bw)
    pairs_fw = compute_and_save(D, Pfw, range_fw)
    pairs_bw = compute_and_save(D, Pbw, range_bw)

    pairs_fw, pairs_bw
end

"""
Save results to HDF5 file.

Each particle set in `pairs` must correspond to a different initial wall
distance.
"""
function save_hdf5(D::Donnees, pairs::Vector{PairSetFinal}, filename::String)
    @assert !PairSets.WALL_UNITS  # wall units not yet supported
    utau, nu = wall_parameters(D)

    # TODO save information about each sphere or shell

    h5open(filename, "w") do io
        init_hdf5(io, pairs[1], utau, nu)
        Ny = length(pairs)
        attrs(io)["num_yplanes"] = length(pairs)
        io["y0+"] = Y0_PLUS
        # TODO add more stuff here

        Gdata = g_create(io, "PairSets")
        for j = 1:Ny
            Gy = g_create(Gdata, "y$j")
            Gy["y0+"] = Y0_PLUS[j]
            Gy["num_pairs"] = pairs[j].Npairs
            save_pair_set(Gy, pairs[j], utau, nu)
        end
    end

    nothing
end

function main()
    D = Donnees(DONNEES_FILE)

    # Define iteration ranges for forwards and backwards dispersion.
    # The idea is to have both forwards and backwards computations start at
    # the same timesteps (so that the exact same particle pairs are tracked in
    # both temporal directions).
    Rall = particle_range(PARTICLE_DIRS)
    Rall_fw = (Rall.start + IT_WINDOW - IT_STEP):IT_STEP:Rall.stop
    ranges_fw = get_subranges(Rall_fw, IT_WINDOW, IT_START_DELTA,
                              allow_shorter=false)
    if isempty(ranges_fw)
        info("Not enough particle data.")
        return
    end
    if TESTING
        ranges_fw = ranges_fw[1:2]
    end
    # ranges_fw = ranges_fw[1:21]
    ranges_bw = [range(r.start, -r.step, length(r)) for r in ranges_fw]
    @assert ranges_bw[1].stop == Rall.start
    @show ranges_fw

    pairs_fw = PairSetFinal[]
    pairs_bw = PairSetFinal[]

    for n in eachindex(ranges_fw)
        # n == 7 && break
        Rfw = ranges_fw[n]
        Rbw = ranges_bw[n]
        print_with_color(:magenta, "\nRanges (fw, bw): [$Rfw, $Rbw]\n")
        Pfw, Pbw = load_or_compute(D, Rfw, Rbw)
        if n == 1
            @assert isempty(pairs_fw)
            append!(pairs_fw, Pfw)
            append!(pairs_bw, Pbw)
        else
            @assert length(pairs_fw) == length(Pfw)
            for j in eachindex(Pfw)
                pairs_fw[j] .+= Pfw[j]
                pairs_bw[j] .+= Pbw[j]
            end
        end
    end

    save_hdf5(D, pairs_fw, "$(OUTDIR)/$(OUTPUT_BASE)_fw.h5")
    save_hdf5(D, pairs_bw, "$(OUTDIR)/$(OUTPUT_BASE)_bw.h5")

    nothing
end

main()
