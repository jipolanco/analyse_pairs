const TESTING = "--test" in ARGS

if TESTING
    info("Testing!")
end

const DATA_DIRS = [joinpath("..", d) for d in readdir("..")
                   if startswith(d, "data.")]
const DONNEES_FILE = "../canal.dat"

const PAIR_INFO_FILE = "pair_info.h5"

const OUTDIR = TESTING ? "output.test" : "output"
mkpath(OUTDIR)
