"""
Identify neighbouring particles using the cell lists algorithm.

<https://en.wikipedia.org/wiki/Cell_lists>
"""
module CellLists

export identify_pairs

struct Particle{T<:Real}
    id :: Int
    x  :: T
    y  :: T
    z  :: T
end

struct Cell{T}
    particles :: Vector{Particle{T}}
    Cell{T}() where T = new(Particle{T}[])
end


"Squared distance between two particles."
particle_distance_sq(p::Particle, q::Particle) =
    (p.x - q.x) * (p.x - q.x) +
    (p.y - q.y) * (p.y - q.y) +
    (p.z - q.z) * (p.z - q.z)


"Initialises and fill cells for pair identification."
function fill_cells(Lxyz_in, particle_xyz::Matrix, Rc, y0, dy)
    const T = eltype(particle_xyz)
    @assert length(Lxyz_in) == 3

    # Note: one necessary condition for a single particle is to be in the range
    # y0 ± (dy + Rc/2). Therefore, we only care about particles in that range.
    ymin::T = y0 - dy - Rc/2
    ymax::T = y0 + dy + Rc/2

    const Lxyz = T.(Lxyz_in)  # Lx, Ly, Lz
    Lxyz[2] = ymax - ymin
    const Dxyz = [L / floor(L/Rc) for L in Lxyz]  # Δx, Δy, Δz
    const Nxyz = round.(Int, Lxyz./Dxyz)  # Nx, Ny, Nz
    @assert Nxyz ≈ Lxyz./Dxyz
    const xyz0 = zeros(T, 3)  # "inferior" boundaries
    xyz0[2] = ymin

    const Npart = size(particle_xyz, 2)

    # Initialise cells, including ghost cells in the three directions.
    # (In y, the ghost cells are empty. They just simplify the code for the case
    # Nxyz = 1).
    cells = Array{Cell{T}}(Nxyz[1]+1, Nxyz[2]+1, Nxyz[3]+1)
    for i in eachindex(cells)
        cells[i] = Cell{T}()
    end

    # Assign a cell to each particle.
    ijk = zeros(Int, 3)
    for p = 1:Npart
        y = particle_xyz[2, p]
        if !(ymin < y < ymax)
            continue
        end
        for n = 1:3
            ijk[n] = ceil(Int, (particle_xyz[n, p] - xyz0[n]) / Dxyz[n])
            @assert 1 <= ijk[n] <= Nxyz[n]
        end

        i = ijk[1]
        j = ijk[2]
        k = ijk[3]
        @assert 1 <= j <= Nxyz[2]

        x = particle_xyz[1, p]
        y = particle_xyz[2, p]
        z = particle_xyz[3, p]

        push!(cells[i, j, k].particles, Particle(p, x, y, z))

        # Also assign ghost cells to account for periodicity.
        # The position of each particle is translated by one period in each
        # periodic direction when it corresponds.
        if i == 1
            c = cells[end, j, k].particles
            push!(c, Particle(p, x + Lxyz[1], y, z))
        end
        if k == 1
            c = cells[i, j, end].particles
            push!(c, Particle(p, x, y, z + Lxyz[3]))
        end
        if i == 1 && k == 1
            c = cells[end, j, end].particles
            push!(c, Particle(p, x + Lxyz[1], y, z + Lxyz[3]))
        end
    end

    cells
end


"Identify particle pairs inside cells."
function identify_pairs_in_cells{T}(cells::Array{Cell{T}}, Rc, y0, dy)
    # Identify particle pairs by iterating over the cells.
    pairs_1D = Int[]
    Mx, My, Mz = size(cells)
    const Rc_sq = Rc * Rc
    for k = 1:Mz-1, j = 1:My-1, i = 1:Mx-1
        parts0 = cells[i, j, k].particles
        isempty(parts0) && continue
        for dk in (0, 1), dj in (0, 1), di in (0, 1)
            samecell = di == dj == dk == 0
            parts = cells[i+di, j+dj, k+dk].particles
            isempty(parts) && continue
            for P0 in parts0, P in parts
                # Make sure that pairs are not repeated. This can only happen if
                # both particles are in the same cell. This also ensures that P0
                # and P are not the same particle.
                samecell && P0.id >= P.id && continue
                yc = 0.5 * (P0.y + P.y)
                abs(yc - y0) > dy && continue
                if particle_distance_sq(P0, P) < Rc_sq
                    # Make sure that both particles are sorted by id.
                    id_min, id_max =
                        ifelse(P0.id < P.id, (P0.id, P.id), (P.id, P0.id))
                    push!(pairs_1D, id_min, id_max)
                end
            end
        end
    end
    Npairs = Int(length(pairs_1D)/2)
    pairs = reshape(pairs_1D, 2, Npairs)

    # Sort particle pairs by id.
    # Pairs are sorted by the id of the first particle.
    # If the ids of two "first" particles are equal, sort them by the id of the
    # second particle.
    # (This corresponds to the default behaviour of `isless` on tuples).
    id_tuples = reinterpret(Tuple{Int,Int}, pairs_1D)
    sort!(id_tuples)

    pairs
end


"""Identify particle pairs using cell lists algorithm.

The domain is divided into (almost) cubic cells with edge sizes close to `Rc`
(but usually sligthly larger).

Looks for particle pairs whose centres are located in the range y0 ± dy, with
y0 ∈ [-1, 1], and whose separation is smaller than Rc.
The default values of `y0` and `dy` corresponds to taking into account the whole
channel width.

Returns (2, Npairs) array of particle pair ids.
"""
function identify_pairs(Lxyz_in::Vector, particle_xyz::Array, Rc::Real;
                        y0::Real=0.0, dy::Real=0.5Lxyz_in[2])
    cells = fill_cells(Lxyz_in, particle_xyz, Rc, y0, dy)
    pairs = identify_pairs_in_cells(cells, Rc, y0, dy)
    @assert size(pairs, 1) == 2
    pairs
end

end
