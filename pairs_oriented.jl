#!/usr/bin/env julia
#
# Forwards dispersion statistics of particle pairs with fixed initial
# orientations and separations.

push!(LOAD_PATH, ".")
using NadiaSpectral: DonneesNS
using ParticleIO
using PairSets

using HDF5

include("constants.jl")
const PARTICLE_DIRS = [joinpath(d, "particle_pairs") for d in DATA_DIRS]

info("Running on $(Threads.nthreads()) threads.")

if !isfile(PAIR_INFO_FILE)
    error("File $PAIR_INFO_FILE not found.")
end

const TOTAL_RANGE = particle_range(PARTICLE_DIRS)
const IT_START = TOTAL_RANGE.start
const IT_RANGE = TESTING ? range(IT_START, 10, 50) : TOTAL_RANGE

const OUTPUT_BASE = "pairs_oriented"
const RESULTS_FILE = joinpath(OUTDIR, "$OUTPUT_BASE.jld")

# Save output in wall units? (otherwise in simulation units)
# const WALL_UNITS = "--wall-units" in ARGS
# if WALL_UNITS
#     info("Saving HDF5 output in wall units.")
# end

# Skip a value of D0/η in the final HDF5 output file.
# The initial separation D0/η = 32 may be unwanted since it's not a power of 4
# (as opposed to the rest of the studied values: 1, 4, 16, 64).
const OUTPUT_SKIP_D0ETA = 32.


function initialise(D::Donnees, pdata::PData)
    # Load particle pair info.
    io = h5open(PAIR_INFO_FILE, "r")
    Ny = read(io, "num_yplanes")
    Nc = read(io, "num_clusters_per_yplane")
    Nr = read(io, "num_initial_separations")
    yplanes = read(io, "y_planes")
    eta = read(io, "eta")
    D0_eta = read(io, "D0_eta")
    close(io)
    @assert length(yplanes) == length(eta) == Ny
    @assert length(D0_eta) == Nr

    particles_per_cluster = 1 + 3Nr

    @assert pdata.Npart == Ny * Nc * particles_per_cluster

    # Ids of particle pair sets that share the same y-plane and the same initial
    # separation vector.
    pairs = Array{Int}(2, Nc)

    # Particle separation vector.
    D0vec = zeros(3)

    Dt = get_deltat(D) * IT_RANGE.step
    Lxyz = get_domain_dimensions(D)

    T = eltype(PData)
    pair_sets = Array{PairSet{T}}(3, Nr, Ny)
    yindex, Umean = init_eulerian(pdata)

    for (j, y) in enumerate(yplanes)
        # Id of the first particle in this y-plane.
        p0 = (j - 1) * Nc * particles_per_cluster + 1
        k = 0  # offset between the indices of the two particles in a pair
        for (m, D0_e) in enumerate(D0_eta)
            D0 = D0_e * eta[j]
            for i = 1:3
                k += 1
                fill!(D0vec, 0.0)
                D0vec[i] = D0
                for c = 1:Nc
                    p = p0 + (c - 1) * particles_per_cluster
                    pairs[1, c] = p
                    pairs[2, c] = p + k
                end
                pair_sets[i, m, j] = PairSet(pdata, pairs, Lxyz, y, D0vec,
                                             IT_RANGE, Dt, Umean, yindex)
            end
        end
    end

    pair_sets
end

"Save all results to HDF5 file."
function save_hdf5(D::Donnees, pairs::Array{T} where T<:PairSet)
    utau, nu = wall_parameters(D)
    const WALL_UNITS = PairSets.WALL_UNITS

    # These are nominal properties of the initial state of particle pairs,
    # in the sense that some of them are be based on quantities such as the
    # dissipation, which were obtained from previous simulations.
    local y0_planes, D0_eta_planes, eta_planes
    h5open(PAIR_INFO_FILE, "r") do io
        y0_planes = read(io, "y_planes")
        D0_eta_planes = read(io, "D0_eta")
        eta_planes = read(io, "eta")
    end
    y0_planes_plus = (1 + y0_planes) * utau / nu

    # Time steps. All PairSet objects must have the same time step.
    const Nt = pairs[1].Nt
    const Dt_sim = pairs[1].Dt

    # Indices of initial separations D0 that will not be skipped.
    D0keep::BitArray = D0_eta_planes .!= OUTPUT_SKIP_D0ETA

    filename =
        @sprintf("%s%s.h5", OUTPUT_BASE, WALL_UNITS ? "_wall_units" : "")
    outfile = joinpath(OUTDIR, filename)
    h5open(outfile, "w") do io
        init_hdf5(io, pairs[1], utau, nu)
        attrs(io)["README"] =
        """
        Particle pair dispersion statistics.

        The data describes the average pair dispersion of tetrads of particles.
        A particle tetrad is composed by a "main" particle initially located at a
        wall distance y0, and 3 "satellite" particles whose initial separation
        distance from the main particle is D0. The initial separation vectors
        between the main and each satellite particle are given by D0*ex, D0*ey and
        D0*ez, respectively, where ex, ey, ez are the unitary vectors in each
        Cartesian coordinate.
        """

        attrs(io)["num_yplanes"] = length(y0_planes_plus)
        attrs(io)["num_initial_separations"] = sum(D0keep)

        io["y0/h"] = y0_planes
        io["y0+"] = y0_planes_plus
        io["D0_eta"] = D0_eta_planes[D0keep]
        attrs(io["D0_eta"])["Description"] = "D0/η"

        # Create HDF groups and write data.
        Gdata = g_create(io, "PairSets")
        for (j, yp) in enumerate(y0_planes_plus)
            Gy = g_create(Gdata, "y$j")
            Gy["y0/h"] = y0_planes[j]
            Gy["y0+"] = yp
            m_group = 1
            for (m, D0_e) in enumerate(D0_eta_planes)
                if !D0keep[m]
                    # info("Output: skipping D0/η = $D0_e")
                    continue
                end
                D0_sim = D0_e * eta_planes[j]
                D0 = D0_sim * (WALL_UNITS ? (utau / nu) : 1.0)
                Gr = g_create(Gy, "r$m_group")
                m_group += 1
                Gr["D0_eta"] = D0_e
                Gr["D0_nominal"] = D0
                for (i, groupname) in enumerate(("x", "y", "z"))
                    P = pairs[i, m, j]
                    y0 = P.initial_stats.y0
                    @assert isapprox(y0, y0_planes[j], rtol=1e-6)
                    @assert isapprox(P.D0_nominal, P.initial_stats.D, rtol=1e-4)
                    @assert norm(P.D0_nominal) ≈ D0_sim
                    @assert P.Dt == Dt_sim && P.Nt == Nt
                    for _j = 1:3
                        # Check that the initial separation of pairs[i] is
                        # oriented in the i-th direction (x, y or z).
                        @assert (_j == i) || (P.D0_nominal[_j] == 0)
                    end
                    g = g_create(Gr, groupname)
                    save_pair_set(g, P, utau, nu)
                end
            end
        end
    end

    nothing
end


function main()
    D = Donnees(DONNEES_FILE)

    resfile = RESULTS_FILE
    if isfile(resfile) && !TESTING
        # Load existent results. Compute missing iterations if the existent
        # results are not up-to-date.
        pairs_in = load_results(resfile)
        iters = pairs_in[1].itrange
        Nt_in = length(iters)
        Nt = length(IT_RANGE)
        info("Loaded $RESULTS_FILE ($Nt_in iterations)")
        if Nt_in < Nt
            info("Computing iterations $(IT_RANGE[Nt_in+1:Nt]) ",
                 "($(Nt - Nt_in) iterations)")
            @time pairs = compute(D, PARTICLE_DIRS, IT_RANGE, pairs_in)
            @time save_results(pairs, resfile)
        else
            @assert Nt_in == Nt
            pairs = pairs_in
        end
    else
        # Compute everything starting from the initial iteration.
        pfile = particle_file(PARTICLE_DIRS, IT_RANGE.start)
        pdata = PData(pfile)
        read_particle_file!(pdata, pfile)
        @time pair_sets = initialise(D, pdata)
        @time pairs = compute(D, PARTICLE_DIRS, IT_RANGE, pair_sets)
        @assert pair_sets === pairs
        @time save_results(pairs, resfile)
    end

    @time save_hdf5(D, pairs)

    nothing
end

main()
